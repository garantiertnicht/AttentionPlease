name := "AttentionPlease"

version := "1.0"

scalaVersion := "3.2.1"

resolvers += Resolver.bintrayRepo("dv8fromtheworld", "maven")
resolvers += Resolver.jcenterRepo

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.15" % "test"
libraryDependencies += "com.github.mpkorstanje" % "tr39-confusables-skeleton" % "8.0.10"
libraryDependencies += "net.dv8tion" % "JDA" % "5.0.0-beta.10"
libraryDependencies += "com.ibm.icu" % "icu4j" % "72.1"
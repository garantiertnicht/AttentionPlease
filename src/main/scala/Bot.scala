/*
 * !AttentionPlease makes nicknames on Discord less toxic.
 * Copyright (C) 2018 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

import net.dv8tion.jda.api.Permission.*
import net.dv8tion.jda.api.entities.MessageActivity.ActivityType

import java.util.concurrent.TimeUnit
import net.dv8tion.jda.api.entities.{Activity, Member, User}
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent
import net.dv8tion.jda.api.events.guild.{GuildJoinEvent, GuildLeaveEvent}
import net.dv8tion.jda.api.events.guild.member.{GuildMemberJoinEvent, GuildMemberRoleAddEvent}
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdatePositionEvent
import net.dv8tion.jda.api.events.session.ReadyEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateNameEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.interactions.commands.DefaultMemberPermissions
import net.dv8tion.jda.api.interactions.commands.build.Commands
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.utils.MemberCachePolicy
import net.dv8tion.jda.api.utils.cache.CacheFlag
import net.dv8tion.jda.api.{JDA, JDABuilder, Permission}

import scala.collection.JavaConverters.*
import scala.collection.mutable
import scala.util.Success
import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.Source

object Bot {
  private var jda_ : JDA = _

  def jda: JDA = jda_

  val commands = Seq("setup-instructions", "what-i-do")
  val commandParsed: Map[String, (String, String)] = Map.from(
    commands.map(name => (name, Source.fromInputStream(getClass.getResourceAsStream(s"/$name.md")).getLines))
      .map {
        case (name: String, source: Iterator[String]) => (
          name,
          (
            source.next().substring(2),
            source.mkString("\n")
          )
        )
      }
  )

  def main(args: Array[String]): Unit = {
    val token = args.head
    jda_ = JDABuilder.create(token, GatewayIntent.GUILD_MEMBERS)
      .addEventListeners(Events)
      .disableCache(CacheFlag.ACTIVITY)
      .disableCache(CacheFlag.CLIENT_STATUS)
      .disableCache(CacheFlag.EMOJI)
      .disableCache(CacheFlag.MEMBER_OVERRIDES)
      .disableCache(CacheFlag.ROLE_TAGS)
      .disableCache(CacheFlag.VOICE_STATE)
      .disableCache(CacheFlag.FORUM_TAGS)
      .disableCache(CacheFlag.ONLINE_STATUS)
      .disableCache(CacheFlag.SCHEDULED_EVENTS)
      .disableCache(CacheFlag.STICKER)
      .setMemberCachePolicy(MemberCachePolicy.ALL)
      .build()
    jda.awaitReady()
    jda.awaitShutdown()
  }

  val guildsNotified = mutable.Map[User, Long]()

  def notify(user: User, message: String, reason: String): Unit = {
    val epoch = System.currentTimeMillis() / (60 * 60 * 1000)
    if (guildsNotified.get(user).contains(epoch)) {
      return
    }

    if (user.isBot) {
      return
    }

    guildsNotified.put(user, epoch)

    val messageToSend =
      s"""Hello, discordUser!
         |
         |$message
         |
         |--
         |You receive this message because you $reason.
         |If you have further questions, please ask at https://discord.gg\\9rx5vG4
         |!AttentionPlease, your friendly neighbourhood bot to eliminate toxic nicknames from Discord.""".stripMargin

    user.openPrivateChannel().queue(channel => {
      channel.sendMessage(messageToSend).queue()
    })
  }

  def checkName(members: Seq[Member]): Unit = {
    if (members.isEmpty) return

    members
      .filter(member => member.getGuild.getSelfMember.canInteract(member))
      .filterNot(member => member.getUser.isBot)
      .sortBy(member => -member.getTimeJoined.toEpochSecond)
      .foreach(member => updateName(member, member.getEffectiveName))
  }

  def updateName(member: Member, name: String): Unit = {
    if (member.getUser.isBot) {
      // Sometimes bots do have crazy prefixes - ignore them.
      return
    }

    Normalize(name).onComplete {
      case Success(goodName) =>
        setName(member, goodName)
      case _ =>
    }
  }

  def setName(member: Member, suggestedName: String): Unit = {
    if (member.getNickname == suggestedName) return

    if (!member.getGuild.getSelfMember.canInteract(member)) {
      return
    }

    if (!member.getGuild.getSelfMember.hasPermission(Permission.NICKNAME_MANAGE)) {
      member.getGuild.getMembers.asScala.filter(_.hasPermission(MANAGE_SERVER, NICKNAME_MANAGE)).foreach(member => notify(
        member.getUser,
        """I have noticed that I do not have permissions to manage nicknames in your guild anymore, at which point
          |I am pretty obsolete. If you need me again, please use the official invite link found in my support guild!""".stripMargin,
        s"manage the server ${member.getGuild.getName} where I have no permissions in."
      ))


      member.getGuild.leave().queueAfter(30, TimeUnit.SECONDS)
      return
    }

    var name = suggestedName
    var i = 128

    def condition(member: Member) =
      member.getGuild.getMembers.asScala.exists(other => name.equalsIgnoreCase(other.getEffectiveName) && member != other)
      || name.length < 2
      || name.length > 32

    def hasDiscriminator(user: User) = user.getDiscriminator.exists(_ != 0)

    if (condition(member) && hasDiscriminator(member.getUser)) {
      // We append a full stop to work around https://github.com/discord/discord-api-docs/issues/6199
      // I am aware that this will clash at 32 characters, but in that case we’ll fall back to a random name for now.
      name = member.getUser.getName + "."
    }

    while (i > 0 && condition(member)) {
      name = RandomName(member.getUser.getIdLong + i)
      i -= 1
      if (name == member.getUser.getName) then
        // We append a full stop to work around https://github.com/discord/discord-api-docs/issues/6199
        name += "."
    }

    if (member.getEffectiveName != name) {
      member.modifyNickname(name).queue()
    } else if (member.getNickname != null && member.getUser.getName == name) {
      member.modifyNickname(null).queue()
    }
  }

  object Events extends ListenerAdapter {

    override def onGuildMemberUpdateNickname(event: GuildMemberUpdateNicknameEvent): Unit =
      checkName(Seq(event.getMember))

    override def onReady(event: ReadyEvent): Unit = {
      jda.updateCommands()
        .addCommands(
          (commandParsed.toSeq.map {
            case (name: String, (description: String, _)) => Commands.slash(name, description)
              .setDefaultPermissions(DefaultMemberPermissions.ENABLED)
              .setGuildOnly(false)

          }): _*
        ).queue()

      checkName(jda.getGuilds.asScala.flatMap(_.getMembers.asScala).toSeq)

      // Invite to support guild - also contains bot invite
      jda.getPresence.setActivity(Activity.watching("discord.gg/9rx5vG4"))
      StatsWriter.writeGuildCount(jda.getGuilds.size)
    }

    override def onSlashCommandInteraction(event: SlashCommandInteractionEvent): Unit = {
      val command = commandParsed.get(event.getInteraction.getName)
      command.foreach {
        case (_, text) => event.getInteraction.reply(text).queue()
      }
    }

    override def onGuildJoin(event: GuildJoinEvent): Unit = {
      StatsWriter.writeGuildCount(jda.getGuilds.size)
      checkName(event.getGuild.getMembers.asScala.toSeq)
    }

    override def onGuildLeave(event: GuildLeaveEvent): Unit = {
      StatsWriter.writeGuildCount(jda.getGuilds.size)
    }

    override def onGuildMemberJoin(event: GuildMemberJoinEvent): Unit = {
      checkName(Seq(event.getMember))
    }

    override def onUserUpdateName(event: UserUpdateNameEvent): Unit = {
      val members = event.getUser.getMutualGuilds.asScala
        .map(_.getMember(event.getUser))
        .filter(_ != null)

      val oldGoodNickname = null

      Normalize(event.getOldName).onComplete {
        case Success(oldGoodName) =>
          members.filter(_ => event.getNewName == null)
            .foreach(member => checkName(Seq(member)))
          members.filter(_ => event.getNewName == oldGoodNickname)
            .foreach(member => updateName(member, event.getUser.getName))
        case _ =>
      }
    }

    override def onRoleUpdatePosition(event: RoleUpdatePositionEvent): Unit = {
      if (event.getGuild.getSelfMember.getRoles.contains(event.getRole)) {
        checkName(event.getGuild.getMembers.asScala.toSeq)
      }
    }

    override def onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent): Unit = {
      if (event.getUser == jda.getSelfUser) {
        checkName(event.getGuild.getMembers.asScala.toSeq)
      }
    }
  }
}
/*
 * !AttentionPlease makes nicknames on Discord less toxic.
 * Copyright (C) 2018 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

import java.io.InputStreamReader
import java.text.Normalizer
import com.github.mpkorstanje.unicode.tr39confusables.Skeleton
import com.ibm.icu.text.Transliterator

import java.util.Properties
import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

object Normalize {
  val moveRegex = "^([^a-zA-Z0-9_.({\\[<]*)(.*)$".r
  val alphaNumericSpace = ((('A' to 'Z') ++: ('a' to 'z')) ++: ('0' to '9')) ++: "_. ".toCharArray.toSeq
  val allowed = alphaNumericSpace ++: Seq(
    '#', '"', '\'', '(', ')', ',',  '*', '+', '-', ':', ';', '<', '>', '?', '[', ']', '{', '|', '}')
  val unicodeNames = alphaNumericSpace.map(character => (character, Character.getName(character.toInt).replaceFirst("LATIN ", "")))
  val notAllowedRegex = "[^a-zA-Z0-9 #\"'(),*+-.:;<>?\\[\\]_{|}]".r
  val unicodeNameRegex = ".*letter ([a-z]+).*".r
  val whitespace = "  +".r
  val replacements = {
    val replacementsProperties = new Properties
    val replacementsStream = getClass.getResourceAsStream("/replacements.properties")
    assert(replacementsStream != null)
    val reader = new InputStreamReader(replacementsStream, "UTF-8")
    replacementsProperties.load(reader)
    reader.close()
    replacementsStream.close()

    replacementsProperties.asScala
  }
  val transliteratString = "Any-Latin; nfd; [:nonspacing mark:] remove; nfc"
  val transliterator = Transliterator.getInstance(transliteratString);

  def apply(name: String) (implicit ex: ExecutionContext): Future[String] = Future {
    val newName = if (name.exists(character => character >= 128 || (!allowed.contains(character)))) {
      whitespace.replaceAllIn(makeFirstCharacterLetter(mapToAscii(Normalizer.normalize(name, Normalizer.Form.NFKD))).trim, " ")
    } else {
      whitespace.replaceAllIn(makeFirstCharacterLetter(name).trim, " ")
    }

    val newNameAlphanumericOnly = newName.map(character => if (alphaNumericSpace.contains(character)) {
      character
    } else {
      ' '
    }).replaceAll("  +", " ").trim

    if (newNameAlphanumericOnly.length > 3 && newNameAlphanumericOnly.length >= (newName.length * 0.65).ceil) {
      newName
    } else {
      newNameAlphanumericOnly
    }
  }

  protected def mapToAscii(name: String): String = {
    val romanized = transliterator.transliterate(name)

    notAllowedRegex.replaceAllIn(romanized, matching => {
      val string = matching.toString
      val character = string.charAt(0)
      applyReplacements(string)
        .orElse(mapByPartialUnicodeName(character))
        .orElse(deconfuse(string))
        .orElse(removeNonAlphanumeric(character))
        .map(_.toString)
        .getOrElse("")
    })
  }

  protected def applyReplacements(character: String): Option[String] = {
    replacements.get(character.toString)
  }

  protected def mapByPartialUnicodeName(character: Char): Option[String] = {
    unicodeNames
      .find(alphaNum => {
        val name = Character.getName(character.toInt)
        name != null && (name.contains(alphaNum._2 + " ") || name.endsWith(alphaNum._2))
      })
      .map(_._1.toString)
  }

  protected def deconfuse(character: String): Option[String] = {
    allowed
      .find(alphaNum => Skeleton.skeleton(character.toString).contains(alphaNum))
      .map(_.toString)
  }

  protected def removeNonAlphanumeric(character: Char): Option[String] = {
    if (character.isUnicodeIdentifierPart){
      None
    } else {
      Some(" ")
    }
  }

  protected def makeFirstCharacterLetter(name: String): String = {
    if (!name.exists(allowed.contains)) {
      ""
    } else {
      moveRegex.replaceFirstIn(name, "$2$1")
    }
  }
}

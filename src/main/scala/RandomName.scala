/*
 * !AttentionPlease makes nicknames on Discord less toxic.
 * Copyright (C) 2018 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

import scala.io.Source
import scala.util.Random

object RandomName {
  val names = {
    val source = Source.fromInputStream(this.getClass.getResourceAsStream("/names"), "UTF-8")
    source.getLines().toSeq
  }

  val firstNames = {
    val source = Source.fromInputStream(this.getClass.getResourceAsStream("/first-names"), "UTF-8")
    source.getLines().toSeq
  }

  protected val random = new Random

  def apply(index: Long): String = {
    val name = names((index % names.length).toInt)
    val firstName = firstNames((index % firstNames.length).toInt)

    s"$firstName $name"
  }
}

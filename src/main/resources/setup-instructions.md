# Instructions on how to set up the bot
It’s pretty simple!
1. Add me to your server.
2. Make sure that the bot role is above all members which should have their name monitored.

⚠️ **This bot starts renaming immediately!**
There is no automated way to revert every changed username. Test out if this bot fulfills your needs before you add it to servers you care about.

That’s it! There are no complicated commands, no settings, no nothing.
If you think that a name is wrong or need more help, feel free to visit our [Support Server](<https://discord.gg/4DZwZA3>).
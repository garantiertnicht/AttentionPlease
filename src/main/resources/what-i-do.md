# Learn what !AttentionPlease has to offer
# What can !AttentionPlease do for you?
I am a bot which eliminates 𝓯𝓪𝓷𝓬𝔂 or ž̷̛̝͆͊̉͗̂̈͑̋̂̿̂̍͘͠a̶̡̭̺͚͍̺̻̫̯͈̟̬͗͐͒͆͊̓̈̿̍̑̕͜͠l̷̨̢̺̭̖̙̜̖͎̜̻͂͛̀̔̓͐́̽̚g̴̞͕̜͕̙̤͋͌̀̆̀̑̽̎̒̃͂̀̋̎̚o̵̧̮̗̬̦̟̐̈̎̓̋͋̍͒́̀̔̎̀͘͝  usernames as well as hoisting and duplicate names from your server! I will make sure that all member names are alphanumeric (and some carefully selected ASCII characters) only. If they aren’t, their names get automagically changed to something closely resembling that.
## Examples
* `! Testificate` becomes `Testificate`
* `𝕿𝖊𝖘𝖙𝖎𝖋𝖎𝖈𝖆𝖙𝖊` becomes `Testificate`
* Names such as `!` or non-unique names will get replaced with their username or a randomly generated name (e.g. Hillie Conkle)
## Get Help
* [Support Server](<https://discord.gg/4DZwZA3>)
* [Source Code (licensed under AGPL 3.0 or later)](<https://gitlab.com/garantiertnicht/AttentionPlease>)
* [Legal fine print (don’t worry)](<https://discord.gg/BhxxJTetPR>)
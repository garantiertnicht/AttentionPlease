/*
 * !AttentionPlease makes nicknames on Discord less toxic.
 * Copyright (C) 2018 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps
import org.scalatest._
import flatspec._

class NormalizeTest extends UnitTest {
  "Normalize" can {
    "normalize unicode" should {
      "not change ASCII" in {
        Await.result(Normalize("hello w0rld_."), 1 second) shouldBe "hello w0rld_."
      }

      "replace mathematical symbols" in {
        // 𝓱𝓮𝓵𝓵𝓸
        Await.result(Normalize("\uD835\uDCF1\uD835\uDCEE\uD835\uDCF5\uD835\uDCF5\uD835\uDCF8"), 1 second) shouldBe "hello"
      }

      "remove accents" in {
        Await.result(Normalize("äâ"), 1 second) shouldBe "aa"
      }

      "handle ☫\uD835\uDD79\uD835\uDD8A\uD835\uDD92\uD835\uDD8A\uD835\uDD98\uD835\uDD8E\uD835\uDD98" in {
        Await.result(Normalize("☫\uD835\uDD79\uD835\uDD8A\uD835\uDD92\uD835\uDD8A\uD835\uDD98\uD835\uDD8E\uD835\uDD98"), 1 second) shouldBe "Nemesis"
      }
    }

    "remove non-ascii characters" should {
      "leave all ascii characters" in {
        Await.result(Normalize("hello world 123"), 1 second) shouldBe "hello world 123"
      }

      "remove non-ascii characters and non-characters" in {
        Await.result(Normalize("¿¿!hello world!¿¿"), 1 second) shouldBe "hello world"
      }

      "return empty string if the character cannot be sanitized" in {
        Await.result(Normalize("¿"), 1 second) shouldBe ""
      }

    }

    "leave tags in place" should {
      "handle braces" in {
        Await.result(Normalize("(t) testificate"), 1 second) shouldBe "(t) testificate"
      }
      "handle curly braces" in {
        Await.result(Normalize("{t} testificate"), 1 second) shouldBe "{t} testificate"
      }
      "handle edgy braces" in {
        Await.result(Normalize("[t] testificate"), 1 second) shouldBe "[t] testificate"
      }
      "handle smaller then/bigger then" in {
        Await.result(Normalize("<t> testificate"), 1 second) shouldBe "<t> testificate"
      }
      "not leave them for short names" in {
        Await.result(Normalize("<<<<<t>>>>>est"), 1 second) shouldBe "t est"
      }
    }

    "move anything but letters from the beginning to the end" should {
      "leave letters at the beginning" in {
        Await.result(Normalize("hello world"), 1 second) shouldBe "hello world"
      }

      "names must start with a valid username character (or tag)" in {
        Await.result(Normalize("::user:name::"), 1 second) shouldBe "user:name::::"
      }

      "leave numbers in place" in {
        Await.result(Normalize("1337"), 1 second) shouldBe "1337"
      }

      "leave underscores in place" in {
        Await.result(Normalize("__"), 1 second) shouldBe "__"
      }

      "leave periods in place" in {
        Await.result(Normalize("._"), 1 second) shouldBe "._"
      }
    }

    "convert cyrillic by closest look" in {
       Await.result(Normalize("АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя"), 1 second) shouldBe
         "AaBbVvGgDdEeEeZzZzIiJjKkLlMmNnOoPpRrSsTtUuFfHhCcCcSsSs''Yy''EeUuAa"
    }

    "works as advertised" should {
      "Testificate" in {
        Await.result(Normalize("! Testificate"), 1 second) shouldBe "Testificate"
      }

      "!" in {
        Await.result(Normalize("!"), 1 second) shouldBe ""
      }

      "\uD835\uDD7F\uD835\uDD8A\uD835\uDD98\uD835\uDD99\uD835\uDD8E\uD835\uDD8B\uD835\uDD8E\uD835\uDD88\uD835\uDD86\uD835\uDD99\uD835\uDD8A" in {
        Await.result(Normalize("\uD835\uDD7F\uD835\uDD8A\uD835\uDD98\uD835\uDD99\uD835\uDD8E\uD835\uDD8B\uD835\uDD8E\uD835\uDD88\uD835\uDD86\uD835\uDD99\uD835\uDD8A"), 1 second) shouldBe "Testificate"
      }

      "(CLaN) Testificate" in {
        Await.result(Normalize("(CLaN) Testificate"), 1 second) shouldBe "(CLaN) Testificate"
      }
    }
  }
}
